package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.exception.InvalidUserInputException;

public class ProductImpl implements Product {
    private static final int NAME_MIN_LENGTH = 3;
    private static final int NAME_MAX_LENGTH = 10;
    private static final int BRAND_MIN_LENGTH = 2;
    private static final int BRAND_MAX_LENGTH = 10;
    private static final int PRICE_MIN_VALUE = 0;
    private static final String INVALID_PRICE_MESSAGE = "Price can't be negative.";
    private static final String INVALID_NAME_MESSAGE = String.format("Product name should be between %d and %d symbols.",NAME_MIN_LENGTH,NAME_MAX_LENGTH);
    private static final String INVALID_BRAND_MESSAGE = String.format("Product brand should be between %d and %d symbols.",BRAND_MIN_LENGTH,BRAND_MAX_LENGTH);
    private String name;
    private String brand;
    private double price;
    private final GenderType gender;

    public ProductImpl(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        //TODO Validate name
        if (name.length() < NAME_MIN_LENGTH || name.length() > NAME_MAX_LENGTH) {
            throw new InvalidUserInputException(INVALID_NAME_MESSAGE);
        }
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        //TODO Validate brand
        if (brand.length() < BRAND_MIN_LENGTH || name.length() > BRAND_MAX_LENGTH) {
            throw new InvalidUserInputException(INVALID_BRAND_MESSAGE);
        }
        this.brand = brand;

    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        //TODO Validate price
        if(price < PRICE_MIN_VALUE){
            throw new IllegalArgumentException(INVALID_PRICE_MESSAGE);
        }
        this.price = price;
    }

    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        return String.format(
                "#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s%n",
                name,
                brand,
                price,
                gender);
    }
}
