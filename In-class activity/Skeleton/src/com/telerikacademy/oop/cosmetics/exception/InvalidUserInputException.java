package com.telerikacademy.oop.cosmetics.exception;

public class InvalidUserInputException extends RuntimeException {

    public InvalidUserInputException(String message) {
        super(message);
    }
}
