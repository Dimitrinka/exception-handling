package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.exception.InvalidUserInputException;

import java.util.List;

public class ShowCategory implements Command {
    private final ProductRepository productRepository;
    private String result;
    public static final int EXPECTED_PARAMETERS_COUNT = 1;
    private static final String INVALID_PARAMETERS_COUNT_MESSAGE = String.format(
            "ShowCategory command expects %d parameters.",
            EXPECTED_PARAMETERS_COUNT);
    public ShowCategory(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void execute(List<String> parameters) {
        //TODO Validate parameters count
        if(parameters.size() < EXPECTED_PARAMETERS_COUNT) {
            throw new InvalidUserInputException(INVALID_PARAMETERS_COUNT_MESSAGE);
        }
        String categoryName = parameters.get(0);
        result = showCategory(categoryName);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String showCategory(String categoryName) {
        //TODO Validate category exist
        if(!productRepository.getCategories().containsKey(categoryName)){
            throw new IllegalArgumentException(String.format("Category %s does not exist.",categoryName));
        }
        Category category = productRepository.getCategories().get(categoryName);

        return category.print();
    }
}
