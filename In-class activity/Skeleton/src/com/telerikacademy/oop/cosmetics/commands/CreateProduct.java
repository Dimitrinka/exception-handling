package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.*;
import com.telerikacademy.oop.cosmetics.exception.DuplicateEntityException;
import com.telerikacademy.oop.cosmetics.exception.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;

import java.util.List;

public class CreateProduct implements Command {
    private static final int EXPECTED_PARAMETERS_COUNT = 4;
    private static final String INVALID_PARAMETERS_COUNT_MESSAGE = String.format(
            "CreateProduct command expects %d parameters.",
            EXPECTED_PARAMETERS_COUNT);
    private static final String PRODUCT_EXIST_MESSAGE = "Product %s already exist.";
    private static final String INVALID_PRICE_MESSAGE = "Third parameter should be price (real number).";
    private static final String INVALID_GENDER_MESSAGE = "Forth parameter should be one of Men, Women or Unisex.";
    private static final String PRODUCT_CREATED = "Product with name %s was created!";
    private final ProductRepository productRepository;
    private final ProductFactory productFactory;
    private String result;

    public CreateProduct(ProductRepository productRepository, ProductFactory productFactory) {
        this.productRepository = productRepository;
        this.productFactory = productFactory;
    }

    @Override
    public void execute(List<String> parameters) {
        //TODO Validate parameters count
        try{
            if(parameters.size() < EXPECTED_PARAMETERS_COUNT) {
                throw new InvalidUserInputException(INVALID_PARAMETERS_COUNT_MESSAGE);
            }
            String name = parameters.get(0);
            String brand = parameters.get(1);
            //TODO Validate price format
            double price;
            try{
               price = Double.parseDouble(parameters.get(2));
            }catch (NumberFormatException e) {
                throw new InvalidUserInputException(INVALID_PRICE_MESSAGE);
            }
            //TODO Validate gender format
            GenderType gender;
            try{
                gender = GenderType.valueOf(parameters.get(3).toUpperCase());
            }catch (IllegalArgumentException e){
                throw new InvalidUserInputException(INVALID_GENDER_MESSAGE);
            }
            result = createProduct(name, brand, price, gender);
        }
        catch (IllegalArgumentException e) {
            result = String.format(e.getMessage());

        }

    }

    @Override
    public String getResult() {
        return result;
    }

    private String createProduct(String name, String brand, double price, GenderType gender) {
        //TODO Ensure category name is unique
        try{
            if(productRepository.getProducts().containsKey(name)){
                throw new DuplicateEntityException(String.format(PRODUCT_EXIST_MESSAGE, name));
            }
            Product product = productFactory.createProduct(name, brand, price, gender);
            productRepository.getProducts().put(name, product);

            return String.format(PRODUCT_CREATED, name);
        }catch (Exception e){
            return String.format("%s",e.getMessage());
        }
    }
}
