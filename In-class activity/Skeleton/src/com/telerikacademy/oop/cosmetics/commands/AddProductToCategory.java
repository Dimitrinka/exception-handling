package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.exception.InvalidUserInputException;

import java.util.List;

public class AddProductToCategory implements Command {
    public static final int EXPECTED_PARAMETERS_COUNT = 2;
    private static final String INVALID_PARAMETERS_COUNT_MESSAGE = String.format(
            "AddProductToCategory command expects %d parameters.",
            EXPECTED_PARAMETERS_COUNT);
    private static final String PRODUCT_ADDED_TO_CATEGORY = "Product %s added to category %s!";

    private final ProductRepository productRepository;
    private String result;

    public AddProductToCategory(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void execute(List<String> parameters) {
        //TODO Validate parameters count
        if (parameters.size() < EXPECTED_PARAMETERS_COUNT) {
            throw new InvalidUserInputException(INVALID_PARAMETERS_COUNT_MESSAGE);
        }

        String categoryNameToAdd = parameters.get(0);
        String productNameToAdd = parameters.get(1);

        result = addProductToCategory(categoryNameToAdd, productNameToAdd);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String addProductToCategory(String categoryName, String productName) {
        //TODO Validate product and category exist
        try{
            if(!productRepository.getCategories().containsKey(categoryName)){
                throw new IllegalArgumentException(String.format("Category %s does not exist.",categoryName));
            }
            Category category = productRepository.getCategories().get(categoryName);
            if(!productRepository.getProducts().containsKey(productName))
            {
                throw new IllegalArgumentException(String.format("Product %s does not exist.",productName));
            }
            Product product = productRepository.getProducts().get(productName);
            category.addProduct(product);
            return String.format(PRODUCT_ADDED_TO_CATEGORY, productName, categoryName);
        }catch (IllegalArgumentException e)
        {
            return String.format(e.getMessage());
        }

    }

}
