package com.telerikacademy.oop.cosmetics.core;

import com.telerikacademy.oop.cosmetics.commands.*;
import com.telerikacademy.oop.cosmetics.commands.CommandType;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CommandFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;

public class CommandFactoryImpl implements CommandFactory {
    private static final String COMMAND_NOT_SUPPORTED_MESSAGE = "Command %s is not supported.";
    @Override
    public Command createCommand(String commandTypeValue, ProductFactory productFactory, ProductRepository productRepository) {
        //TODO Validate command format
        CommandType commandType = tryParseCommandType(commandTypeValue);

        switch (commandType) {
            case CREATECATEGORY:
                return new CreateCategory(productRepository, productFactory);
            case CREATEPRODUCT:
                return new CreateProduct(productRepository, productFactory);
            case ADDPRODUCTTOCATEGORY:
                return new AddProductToCategory(productRepository);
            case SHOWCATEGORY:
                return new ShowCategory(productRepository);
            default:
                //TODO Can we improve this code?
                throw new UnsupportedOperationException(String.format(COMMAND_NOT_SUPPORTED_MESSAGE, commandTypeValue));
        }
    }

    private CommandType tryParseCommandType(String value) {
        try {
            return CommandType.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(String.format(COMMAND_NOT_SUPPORTED_MESSAGE, value));
        }
    }
}



