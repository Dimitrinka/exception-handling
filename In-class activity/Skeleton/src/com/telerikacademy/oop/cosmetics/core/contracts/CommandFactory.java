package com.telerikacademy.oop.cosmetics.core.contracts;

public interface CommandFactory {
    Command createCommand(String commandType, ProductFactory productFactory, ProductRepository productRepository);
}
