package com.telerikacademy.oop.cosmetics.core.contracts;

import com.telerikacademy.oop.cosmetics.models.GenderType;

public interface ProductFactory {
    Category createCategory(String name);

    Product createProduct(String name, String brand, double price, GenderType gender);
}
